
from misc import createBipartite

import copy
from algo import edge_coloring
import numpy as np
from human import doHuman
from hw import genTSIMem
from hw import genCrossbarMem



def getCoor(x,y, n, fs):
	#print "Coor: ", x,':', y
	coor = {}
	int = n/fs

	start = 1
	end = int

	xcoor = ""
	ycoor = ""

	for i in range(0,fs):
		#print "check int:", start,end

		if start <= x <= end:
			#print "x"+str(i)
			xcoor = "x"+str(i)


		if start <= y <= end:
			#print "y"+str(i)
			ycoor = "y" + str(i)
		start += int
		end += int


	coor[xcoor] = ycoor
	return coor
def checkKeyValuePairExistence(dic, key, value):
    try:
        return dic[key] == value
    except KeyError:
        return False


#inOut = genInputOutput(1600)
#np.save('inOut_file.npy', inOut)
#print "1 :",inOut[1]

#inOut = np.load('inOut_file.npy').item()




inOut = doHuman()
#inOut = {1:7, 2: 4, 3: 10, 4: 1, 5: 9, 6: 5, 7: 8, 8: 6, 9: 3, 10: 11, 11: 2, 12: 12}


#print inOut

k = 3

#graph = createBipartite(inOut,80)
graph = createBipartite(inOut, 80)
#print graph
solutions = edge_coloring(graph)
ts_cs = copy.deepcopy(solutions)
fs_cs = copy.deepcopy(solutions)
cs_cs = copy.deepcopy(solutions)

#print solutions


def calculateFS(sol,cfg, N, fs):
	intfs = {}

	start = 0
	end = N/fs

	for i in inOut:

		coortest = getCoor(i, inOut[i], N, fs)
		lenx = len(str(coortest.keys())) - 2
		leny = len(str(coortest.values())) - 2
		x = str(coortest.keys())[2:lenx]
		y = str(coortest.values())[2:leny]
		print x,y

		for j in sol:
			if (checkKeyValuePairExistence(sol[j], x, y)):
				# print j

				sol[j].pop(x)
				break

		if start <= i <= end:
			start += fs
			end += fs

		intfs[i] = j
		#print len(intfs)

	return intfs
def calculateTS(sol, cfg, N, ts):
	intts = {}

	for i in cfg:
		#print cfg[i]
		coortest = getCoor(i, cfg[i], N, ts)

		lenx = len(str(coortest.keys())) - 2
		leny = len(str(coortest.values())) - 2
		x = str(coortest.keys())[2:lenx]
		y = str(coortest.values())[2:leny]
		#print i, cfg[i], x, y

		for j in sol:
			if (checkKeyValuePairExistence(sol[j], x, y)):
				#print "True"
				sol[j].pop(x)

				break


		#print cfg[i],j
		intts[cfg[i]] = j
		intts[i] = j
		#print "test", i,j, len(intts)


	return intts






def calculateStage1(sol, input, k):
	intfs = {}
	intts = {}
	#print ""

	for i in input:
		coortest = getCoor(i, input[i],1600,20)
		#print i , input[i],"->", coortest
		lenx = len(str(coortest.keys())) - 2
		leny = len(str(coortest.values())) - 2
		x = str(coortest.keys())[2:lenx]
		y = str(coortest.values())[2:leny]
		#print x,y

		for j in sol:
			if (checkKeyValuePairExistence(sol[j], x, y)):
				#print j
				#print i , "->", j
				#print j, "->", input[i]
				#print checkSwitch(i,3)
				sol[j].pop(x)
				break
		intfs[i] = j
		intts[input[i]] = j
		#intts[]


	return intfs,intts
fs_cal,ts_cal = calculateStage1(fs_cs, inOut,k)

#print fs_cal
#print ts_cal




#test = calculateFS(solutions, inOut, len(inOut), 20)
#testTS = calculateTS(ts_cs, inOut, len(inOut),20)

#print len(test)
#print len(testTS)

#testomg = calculateFS(ts_cs, inOut, len(inOut), 20)
#print len(testomg)


#print testTS
#print len(testTS)
def toTS(dict,tsCnt):
	start = 1
	end = len(dict)/tsCnt
	int = end
	ts = {}

	for i in range(0,tsCnt):
		toTS = {}
		for key,value in dict.iteritems():
			if start <= key <= end:
				toTS[key] = value
		ts["ts"+str(i)] = toTS
		start += int
		end += int

	return ts

def toFS(dict,fsCnt):
	start = 1
	end = len(dict)/fsCnt
	int = end
	fs = {}

	for i in range(0,fsCnt):
		toFS = {}
		for key,value in dict.iteritems():
			if start <= key <= end:
				toFS[key] = value
				#print key
		fs["fs"+str(i)] = toFS
		start += int
		end += int

	return fs


fs = toFS(fs_cal,20)
ts = toTS(ts_cal,20)


#print fs
#print cs_cs

#print inOut
print inOut
printOut = 1

#print fs.get("fs0")
#genTSIMem("1",fs.get("fs1"))

if(printOut):
	print "--------------------------------------------------------"
	print "                          FS - Stage 1"
	print "--------------------------------------------------------"
	for i in fs:
		print i
		print fs[i]
		#genTSIMem(i[2:],fs[i])
	print "--------------------------------------------------------"
	print "                          CS - Stage 2"
	print "--------------------------------------------------------"
	#genCrossbarMem("crossbar", cs)
	for i in cs_cs:
		print i
		print cs_cs[i]
	print "--------------------------------------------------------"
	print "                          TS - Stage 3"
	print "--------------------------------------------------------"
	cnt = 20
	for i in ts:
		print i
		print ts[i]
		#genTSIMem(str(cnt), ts[i])
	#	cnt += 1
	print "--------------------------------------------------------"




