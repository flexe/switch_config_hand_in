import collections
import operator
from natsort import natsort

def nomalify(fs):

    nfs = {}

    #for i in range(0,80):
        #print i

    cnt = 0
    for key in sorted(fs):
        #print "%s: %s" % (cnt, fs[key])

        #print cnt, fs[key]
        nfs[cnt] = fs[key]
        cnt += 1

    return nfs

def genControl(d):

	controlString = ""


	sorted_dict = collections.OrderedDict(natsort.natsorted(d.items(),key=operator.itemgetter(1)))

	for key, value in sorted_dict.items():
            #controlString += key[1:]
		#print key, value
        #controlString += "d"
		    controlString += "{0:05b}".format(int(key[1:]))
        #controlString += key[1:]

        #controlString += ", "


	return controlString

def genTSIMemStage1(name,input,input2,rx):

    if(rx == 1):
        filename = "mem/rx/tsi_ram_" + name + ".mem"
        side = "rx"
    else:
        filename = "mem/tx/tsi_ram_" + name + ".mem"
        side = "uut"

    file = open(filename, 'w+')
    file.write("// memory data file (do not edit the following line - required for mem load use)\n")
    lineName = "// instance=/tb_tx_top/" + side + "/tsi" + name + "/dual_port_ram_rptr/ram\n"
    file.write(lineName)
    file.write("// format=mti addressradix=d dataradix=b version=1.0 wordsperline=1\n")
    #print input
    norm = nomalify(input)
    norm2 = nomalify(input2)

    norm_rev = dict((y, x) for x, y in norm.iteritems())
    norm_rev2 = dict((y, x) for x, y in norm2.iteritems())
    #print norm_rev

    for i in range(0,80):
        #output =
        #print i
        output = str(i) + ": " + '{0:08b}'.format(int(str(norm_rev.get("cs"+str(i))))) + "\n" #str(norm_rev.get("cs"+str(i)))
        #output = str(i) + ": " + str(norm_rev.get("cs" + str(i))) + "\n"  # str(norm_rev.get("cs"+str(i)))
        #print norm_rev.get("cs"+str(i))
        file.write(output)

        #print output

    addr = 80
    for j in range(0,80):
        output = str(addr) + ": " + '{0:08b}'.format(int(str(norm_rev2.get("cs"+str(j))))) + "\n"
        file.write(output)
        addr+=1
    #for j in range(0,80):
    #    output = str(addr) + ": 00000000\n"
    #    addr += 1
    #    file.write(output)



    return 1

def genTSIMemStage3(name, input,input2,rx):

    if(rx == 1):
        filename = "mem/rx/tsi_ram_" + str(int(name) + 20) + ".mem"
        side = "rx"
    else:
        filename = "mem/tx/tsi_ram_" + str(int(name) + 20) + ".mem"
        side = "uut"
    norm_rev = nomalify(input)
    norm_rev2 = nomalify(input2)
    #print int(name)+20


    file = open(filename, 'w+')
    file.write("// memory data file (do not edit the following line - required for mem load use)\n")
    lineName = "// instance=/tb_tx_top/" + side + "/tsi" + name + "/dual_port_ram_rptr/ram\n"
    file.write(lineName)
    file.write("// format=mti addressradix=d dataradix=b version=1.0 wordsperline=1\n")

    cnt = 0
    for i in range(0, 80):
        #print cnt, input.get(i)[2:]
        output = str(cnt) + ": " + '{0:08b}'.format(int(str(norm_rev.get(i)[2:]))) + "\n"
        file.write(output)
        #print output
        cnt += 1

    for j in range(0,80):
        output = str(cnt) + ": " + '{0:08b}'.format(int(str(norm_rev2.get(j)[2:]))) + "\n"
        file.write(output)
        cnt += 1
    return 1

def genCrossbarMem(name,input, input2,rx):

    if(rx == 1):
        filename = "mem/rx/crossbar_ram.mem"
        side = "rx"
    else:
        filename = "mem/tx/crossbar_ram.mem"
        side = "uut"

    sorted_dict = collections.OrderedDict(natsort.natsorted(input.items(), key=operator.itemgetter(0)))
    sorted_dict2 = collections.OrderedDict(natsort.natsorted(input2.items(), key=operator.itemgetter(0)))

    file = open(filename, 'w+')

    file.write("// memory data file (do not edit the following line - required for mem load use)\n")
    #file.write("// instance=/tb_ctl_crossbar/uut/dual_port_ram/ram\n")
    file.write("// instance=/tb_tx_top/" + side + "/ctl_crossbar0/dual_port_ram/ram\n")
    file.write("// format=mti addressradix=d dataradix=b version=1.0 wordsperline=1\n")

    #print len(ordInput)

    addr = 0
    for key, value in sorted_dict.items():
        #print key, value
        singlecfg = str(addr) + ": " + genControl(value) + "\n"
        #singlecfg = genControl(value)
        #singlecfg += "\n"
        file.write(singlecfg)
        addr += 1

    for key, value in sorted_dict2.items():
        singlecfg = str(addr) + ": " + genControl(value) + "\n"
        file.write(singlecfg)
        addr += 1





    return 1




