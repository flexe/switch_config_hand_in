import copy
from misc import *
from algo import *
from config import *
from hw import *
import time


## TX

# Read two configurations:
inOut = doHuman("cfg.txt")
inOut2 = doHuman("cfg2.txt")

# Create bipartite graph for input/output
graph = createBipartite(inOut, 80)
graph2 = createBipartite(inOut2, 80)


# Find middle switch configurations for cfg0
solutions = edge_coloring(graph)
solution_copy = copy.deepcopy(solutions)
cs_cs = copy.deepcopy(solutions)

# Find middle switch configurations for cfg1
solutions2 = edge_coloring(graph2)
solution_copy2 = copy.deepcopy(solutions2)
cs_cs2 = copy.deepcopy(solutions2)

# Find first and third stage switch configurations for cfg0
fs_cal,ts_cal = calculateStage13(solution_copy, inOut)

# Find first and third stage switch configurations for cfg1
fs_cal2, ts_cal2 = calculateStage13(solution_copy2,inOut2)



# Fixing order of dicts.
fs = toFS(fs_cal,20)
ts = toTS(ts_cal,20)

fs2 = toFS(fs_cal2,20)
ts2 = toTS(ts_cal2,20)





## RX

# Same procedure as TX
outIn = dict((y,x) for x,y in inOut.iteritems())
outIn2 = dict((y,x) for x,y in inOut2.iteritems())


graph3 = createBipartite(outIn, 80)
graph4 = createBipartite(outIn2, 80)


rx_solutions = edge_coloring(graph3)
rx_solution_copy = copy.deepcopy(rx_solutions)
rx_cs_cs = copy.deepcopy(rx_solutions)


rx_solutions2 = edge_coloring(graph4)
rx_solution_copy2 = copy.deepcopy(rx_solutions2)
rx_cs_cs2 = copy.deepcopy(rx_solutions2)


rx_fs_cal,rx_ts_cal = calculateStage13(rx_solution_copy, outIn)

rx_fs_cal2, rx_ts_cal2 = calculateStage13(rx_solution_copy2,outIn2)

rx_fs = toFS(rx_fs_cal,20)
rx_ts = toTS(rx_ts_cal,20)

rx_fs2 = toFS(rx_fs_cal2,20)
rx_ts2 = toTS(rx_ts_cal2,20)




tx = 1
rx = 1

# Print out and mem generation
if(tx):
	print "--------------------------------------------------------"
	print "                        TX                              "
	print "--------------------------------------------------------"
	print "                          FS - Stage 1"
	print "--------------------------------------------------------"
	for i in fs:
		print i
		print fs[i]
		genTSIMemStage1(i[2:],fs[i],fs2[i],0)
	print "--------------------------------------------------------"
	print "                          CS - Stage 2"
	print "--------------------------------------------------------"
	genCrossbarMem("crossbar", cs_cs, cs_cs2,0)
	for i in cs_cs:
		print i
		print cs_cs[i]
	print "--------------------------------------------------------"
	print "                          TS - Stage 3"
	print "--------------------------------------------------------"
	#cnt = 20
	for i in ts:
		print i
		print ts[i]
		genTSIMemStage3(i[2:], ts[i],ts2[i],0)
		#cnt += 1
	print "--------------------------------------------------------"

if(rx):
	print "--------------------------------------------------------"
	print "                        RX                              "
	print "--------------------------------------------------------"
	print "                          FS - Stage 1"
	print "--------------------------------------------------------"
	for i in rx_fs:
		print i
		print rx_fs[i]
		genTSIMemStage1(i[2:],rx_fs[i],rx_fs2[i],1)
	print "--------------------------------------------------------"
	print "                          CS - Stage 2"
	print "--------------------------------------------------------"
	genCrossbarMem("crossbar", rx_cs_cs, rx_cs_cs2,1)
	for i in rx_cs_cs:
		print i
		print rx_cs_cs[i]
	print "--------------------------------------------------------"
	print "                          TS - Stage 3"
	print "--------------------------------------------------------"
	#cnt = 20
	for i in rx_ts:
		print i
		print rx_ts[i]
		genTSIMemStage3(i[2:], rx_ts[i],rx_ts2[i],1)
		#cnt += 1
	print "--------------------------------------------------------"









