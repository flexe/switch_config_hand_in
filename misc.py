import random

def random_shuffle(dictIn):
    keys = dictIn.keys()
    random.shuffle(keys)
    b = dict(zip(keys, dictIn.values()))
    return b

def genInputOutput(number):
    value = {}
    for i in range(1,number+1):
        value[i] = i
    value = random_shuffle(value)
    #print "p",value
    return value

def getYblock(input, n, no):

    yBlock = []



    stop = no/n
    #print stop
    #print input
    start = 1
    end = n
    for i in range(0,stop):
        #print "range:", start, ",", end


        for L in input:
            #print "Checking input:", L
            #print "end", end
            #print "start", start
            if start <= L <= end:
                test = 0

                #print "input:", L, "Yblock:", 'y' + str(i)
                yBlock.append('y'+str(i))
                #print 'y' + str(i)

        start = start + n
        end = end + n

    return yBlock

def validGraph(input, n):

    if(input%n <= 0):
        return True

    return False

def createBipartite(inOut, n):

    #print validGraph(len(inOut),n)

    if(validGraph(len(inOut),n) == False):
        print "Invalid graph"
        return

    graph = {}
    D = len(inOut)/n


    j = 0

    start = 0
    end = n
    while(j < D):
        #print 'x'+str(j)

        x = dict(inOut.items()[start:end])

        start = start + n
        end = end + n

        y = []
        for key in x:
            y.append(x.get(key))

        #print y

        #print y,n,len(inOut)

        z = getYblock(y,n,len(inOut))
        #print z
        #print "-----"
        graph['x'+str(j)] = z




        j = j+1







    return graph

def getCoor(x,y, n, fs):
	#print "Coor: ", x,':', y
	coor = {}
	int = n/fs

	start = 1
	end = int

	xcoor = ""
	ycoor = ""

	for i in range(0,fs):
		#print "check int:", start,end

		if start <= x <= end:
			#print "x"+str(i)
			xcoor = "x"+str(i)


		if start <= y <= end:
			#print "y"+str(i)
			ycoor = "y" + str(i)
		start += int
		end += int


	coor[xcoor] = ycoor
	return coor

def checkKeyValuePairExistence(dic, key, value):
    try:
        return dic[key] == value
    except KeyError:
        return False

def calculateStage13(sol, input):
	intfs = {}
	intts = {}

	for i in input:
		coortest = getCoor(i, input[i],1600,20)
		lenx = len(str(coortest.keys())) - 2
		leny = len(str(coortest.values())) - 2
		x = str(coortest.keys())[2:lenx]
		y = str(coortest.values())[2:leny]

		for j in sol:
			if (checkKeyValuePairExistence(sol[j], x, y)):
				sol[j].pop(x)
				break

		intfs[i] = j
		intts[input[i]] = j

	return intfs,intts

def toTS(dict,tsCnt):
	start = 1
	end = len(dict)/tsCnt
	int = end
	ts = {}

	for i in range(0,tsCnt):
		toTS = {}
		for key,value in dict.iteritems():
			if start <= key <= end:
				toTS[key] = value
		ts["ts"+str(i)] = toTS
		start += int
		end += int

	return ts

def toFS(dict,fsCnt):
	start = 1
	end = len(dict)/fsCnt
	int = end
	fs = {}

	for i in range(0,fsCnt):
		toFS = {}
		for key,value in dict.iteritems():
			if start <= key <= end:
				toFS[key] = value
				#print key
		fs["fs"+str(i)] = toFS
		start += int
		end += int

	return fs