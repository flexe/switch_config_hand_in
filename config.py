from collections import Counter
import re

def calculateChValue(input):

    list = []



    init = input + 1

    for i in range(0,20):

        list.append(init)
        init += 80

    return list

def calculateLaneValue(phy,lane):

    list = []

    phyList = [0,400,800,1200]

    laneList = [1,81,161,241,321,2,82,162,242,322,3,83,163,243,323,4,84,164,244,324]

    init = phyList[phy] + laneList[lane]



    #print init

    for i in range(0,20):

        list.append(init)


        init+=4


    #print list
    return list

def calculateCFG(input, outputPhy,outputLane):

    dict = {}

    chValues = calculateChValue(input)
    laneValues = calculateLaneValue(outputPhy,outputLane)
    #print chValues
    #print laneValues

    if(len(chValues)==len(laneValues)):
        for i in range(0,20):
            dict[chValues[i]] = laneValues[i]

    return dict

def readInputOutFromTxt(input):

    cfgIn = {}
    with open(input) as f:
        nu = 0

        for line in f:
            values = line.rstrip('\n').split(",")
            values = [int(i) for i in values]

            cfgIn[nu] = values

            nu += 1

    return cfgIn

def calulateOutputCfg(cfgIn):

    cfgOut = {}

    for i in range(0, len(cfgIn)):
        inputCh = cfgIn.get(i)[0]
        outputPhy = cfgIn.get(i)[1]
        outputLane = cfgIn.get(i)[2]


        tempOut = calculateCFG(inputCh, outputPhy, outputLane)

        cfgOut.update(tempOut)

    return cfgOut

def doHuman(name):
    cfg = readInputOutFromTxt(name)


    outCfg = calulateOutputCfg(cfg)

    return outCfg




